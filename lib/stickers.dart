library stickers;

/// Stickers manager.
class Stickers {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}
