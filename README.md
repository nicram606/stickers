# stickers

Translate, scale and rotate any widget with gestures.

## Roadmap
- [ ] Moving widgets with one pointer
- [ ] Translating, scaling and rotating at once with two pointer gesture
- [ ] Deleting by dragging manipulated widget to "Trash" zone

### Other assumptions
- [ ] Two pointer gestures should have effect even if second pointer is not over manipulated widget
- [ ] Scaling should change widget's size to allow to rerenders in full resolution rather than stretching rendered image
- [ ] Every part of the UI should be a replaceble widget with at least one default option for each element
